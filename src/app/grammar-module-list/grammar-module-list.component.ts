import { Component, Input, OnInit } from '@angular/core';
import { GrammarLesson } from '../schemas/grammar-lesson';
import { GrammarLessonService } from '../grammar-lesson.service';

@Component({
  selector: 'grammar-module-list',
  templateUrl: './grammar-module-list.component.html',
  styleUrls: ['./grammar-module-list.component.css'],
})
export class GrammarModuleListComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  @Input()
  lessons: GrammarLesson[] = [];
}
