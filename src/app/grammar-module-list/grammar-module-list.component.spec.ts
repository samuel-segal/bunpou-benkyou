import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrammarModuleListComponent } from './grammar-module-list.component';

describe('GrammarModuleListComponent', () => {
  let component: GrammarModuleListComponent;
  let fixture: ComponentFixture<GrammarModuleListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GrammarModuleListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrammarModuleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
