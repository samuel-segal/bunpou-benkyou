import { Injectable } from '@angular/core';
import { GrammarLesson } from './schemas/grammar-lesson';

@Injectable({
  providedIn: 'root',
})
export class GrammarLessonService {
  constructor() {}

  getLessons(): GrammarLesson[] {
    return [
      {
        japaneseStructure: '~て始める',
        englishStructure: 'To start doing something',
        id: '0000',
      },
      {
        japaneseStructure: '～られる',
        englishStructure: 'To have done unto',
        id: '6160bf31e877290c07edbc62',
      },
      {
        japaneseStructure: '何々',
        englishStructure:
          'An especially long and arduous explanation of the above grammatical concept that transcends practicality',
        id: '0002',
      },
      {
        japaneseStructure: 'が好き',
        englishStructure: 'To like something',
        id: '0003',
      },
      {
        japaneseStructure: 'っている',
        englishStructure: 'To be doing something',
        id: '0004',
      },
    ];
  }
}
