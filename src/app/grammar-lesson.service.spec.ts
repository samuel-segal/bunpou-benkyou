import { TestBed } from '@angular/core/testing';

import { GrammarLessonService } from './grammar-lesson.service';

describe('GrammarLessonService', () => {
  let service: GrammarLessonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GrammarLessonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
