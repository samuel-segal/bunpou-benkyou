import { TestBed } from '@angular/core/testing';

import { LessonPageService } from './lesson-page.service';

describe('LessonPageService', () => {
  let service: LessonPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LessonPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
