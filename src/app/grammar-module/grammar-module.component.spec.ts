import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrammarModuleComponent } from './grammar-module.component';

describe('GrammarModuleComponent', () => {
  let component: GrammarModuleComponent;
  let fixture: ComponentFixture<GrammarModuleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GrammarModuleComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrammarModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
