import { Component, Input, OnInit } from '@angular/core';
import { GrammarLesson } from '../schemas/grammar-lesson';

@Component({
  selector: 'grammar-module',
  templateUrl: './grammar-module.component.html',
  styleUrls: ['./grammar-module.component.css'],
})
export class GrammarModuleComponent implements OnInit {
  @Input() lessonType?: GrammarLesson;

  constructor() {}

  ngOnInit(): void {}
}
