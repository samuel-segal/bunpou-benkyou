import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObjectId } from 'bson';
import { Observable } from 'rxjs';
import { HttpCacheService } from './http-cache.service';
import { LessonPage } from './schemas/lesson-page';

@Injectable({
  providedIn: 'root',
})
export class LessonPageService {
  constructor(private http: HttpCacheService) {}

  getLessonPage(id: string): Observable<LessonPage> {
    return this.http.get<LessonPage>(`api/lessons/${id}`);
  }
}
