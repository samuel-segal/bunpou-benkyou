import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpCacheService {

  constructor(private httpClient: HttpClient) { }

  get<T>(url: string) : Observable<T>{
    const loaded : string = localStorage.getItem(url) ?? '';
    //Return cached result
    if(loaded){
      const json = JSON.parse(loaded);
      return of(json);
    }

    const obs: Observable<T> =  this.httpClient.get<T>(url);
    obs.subscribe( (obj: T)=>{
      const str = JSON.stringify(obj);
      localStorage.setItem(url, str);
    });

    return obs;
  }
}
