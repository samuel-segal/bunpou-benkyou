import { Component, Input, OnInit } from '@angular/core';
import { ExampleSentenceService } from '../example-sentence.service';
import { ExampleSentence } from '../schemas/exampleSentence';

@Component({
  selector: 'example-sentence',
  templateUrl: './example-sentence.component.html',
  styleUrls: ['./example-sentence.component.css'],
})
export class ExampleSentenceComponent implements OnInit {
  @Input()
  exampleSentence?: ExampleSentence;

  constructor(private exampleService: ExampleSentenceService) {}

  ngOnInit(): void {}
}
