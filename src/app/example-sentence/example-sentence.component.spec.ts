import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleSentenceComponent } from './example-sentence.component';

describe('ExampleSentenceComponent', () => {
  let component: ExampleSentenceComponent;
  let fixture: ComponentFixture<ExampleSentenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExampleSentenceComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleSentenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
