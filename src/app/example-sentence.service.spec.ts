import { TestBed } from '@angular/core/testing';

import { ExampleSentenceService } from './example-sentence.service';

describe('ExampleSentenceService', () => {
  let service: ExampleSentenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExampleSentenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
