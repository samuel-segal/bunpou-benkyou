import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpCacheService } from './http-cache.service';
import { ExampleSentence } from './schemas/exampleSentence';

@Injectable({
  providedIn: 'root',
})
export class ExampleSentenceService {
  constructor(private httpClient: HttpCacheService) {}

  getExamples(
    id: string,
    numExamples: number = 1
  ): Observable<ExampleSentence[]> {
    return this.httpClient.get<ExampleSentence[]>(
      `api/examples/${id}/${numExamples}`
    );
  }
}
