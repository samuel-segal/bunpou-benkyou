import { ObjectId } from 'bson';

export interface LessonPage {
  _id: ObjectId;
  title: string;
  content: string;
}
