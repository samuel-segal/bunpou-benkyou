export interface GrammarLesson {
  japaneseStructure: string;
  englishStructure: string;
  jlpt?: number;

  id: string;
}
