export interface ExampleSentence {
  japanese: string;
  english: string;
}
