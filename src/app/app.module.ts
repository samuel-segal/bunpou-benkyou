import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GrammarModuleComponent } from './grammar-module/grammar-module.component';
import { GrammarModuleListComponent } from './grammar-module-list/grammar-module-list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { GrammarModuleMatrixComponent } from './grammar-module-matrix/grammar-module-matrix.component';
import { LessonPageComponent } from './lesson-page/lesson-page.component';
import { HttpClientModule } from '@angular/common/http';
import { ExampleSentenceComponent } from './example-sentence/example-sentence.component';

@NgModule({
  declarations: [
    AppComponent,
    GrammarModuleComponent,
    GrammarModuleListComponent,
    NavbarComponent,
    GrammarModuleMatrixComponent,
    LessonPageComponent,
    ExampleSentenceComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
