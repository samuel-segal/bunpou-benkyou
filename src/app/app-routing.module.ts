import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrammarModuleListComponent } from './grammar-module-list/grammar-module-list.component';
import { GrammarModuleMatrixComponent } from './grammar-module-matrix/grammar-module-matrix.component';
import { LessonPageComponent } from './lesson-page/lesson-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/lessons', pathMatch: 'full' },
  { path: 'lessons', component: GrammarModuleMatrixComponent },
  { path: 'lesson/:id', component: LessonPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
