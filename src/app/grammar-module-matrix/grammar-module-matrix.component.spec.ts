import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrammarModuleMatrixComponent } from './grammar-module-matrix.component';

describe('GrammarModuleMatrixComponent', () => {
  let component: GrammarModuleMatrixComponent;
  let fixture: ComponentFixture<GrammarModuleMatrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GrammarModuleMatrixComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrammarModuleMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
