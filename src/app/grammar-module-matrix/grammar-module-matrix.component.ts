import { Component, OnInit } from '@angular/core';
import { GrammarLesson } from '../schemas/grammar-lesson';
import { GrammarLessonService } from '../grammar-lesson.service';

@Component({
  selector: 'grammar-module-matrix',
  templateUrl: './grammar-module-matrix.component.html',
  styleUrls: ['./grammar-module-matrix.component.css'],
})
export class GrammarModuleMatrixComponent implements OnInit {
  maxLessonColumns: number = 4;

  lessons?: GrammarLesson[];
  lessonLists: GrammarLesson[][] = new Array(this.maxLessonColumns);

  constructor(private grammarLessonService: GrammarLessonService) {}

  ngOnInit(): void {
    this.getLessons();
  }

  getLessons(): void {
    this.lessons = this.grammarLessonService.getLessons();
    this.lessonLists = new Array(4);

    this.lessons.forEach((lesson, index) => {
      const column = index % this.maxLessonColumns;

      if (!this.lessonLists[column]) {
        this.lessonLists[column] = new Array(0);
      }

      const lessonList = this.lessonLists[column];

      lessonList.push(lesson);

      console.log('it works');
    });
  }
}
