import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LessonPage } from '../schemas/lesson-page';
import { LessonPageService } from '../lesson-page.service';
import { ExampleSentence } from '../schemas/exampleSentence';
import { ExampleSentenceService } from '../example-sentence.service';

@Component({
  selector: 'lesson-page',
  templateUrl: './lesson-page.component.html',
  styleUrls: ['./lesson-page.component.css'],
})
export class LessonPageComponent implements OnInit {
  lessonPage?: LessonPage;
  examples?: ExampleSentence[];

  constructor(
    private route: ActivatedRoute,
    private lessonPageService: LessonPageService,
    private exampleService: ExampleSentenceService
  ) {}

  ngOnInit(): void {
    this.getLessonPage();
    this.getExamples(3);
  }

  getLessonPage(): void {
    const id: string = this.route.snapshot.paramMap.get('id') ?? 'null';
    this.lessonPageService.getLessonPage(id).subscribe((lp: LessonPage) => {
      this.lessonPage = lp;
    });
  }

  getExamples(numExamples: number): void {
    const id: string = this.route.snapshot.paramMap.get('id') ?? 'null';
    this.exampleService
      .getExamples(id, numExamples)
      .subscribe((ex: ExampleSentence[]) => {
        this.examples = ex;
      });
  }
}
