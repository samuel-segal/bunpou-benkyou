const mongo = require("mongodb");
const config = require("../config.js");

//TODO IMPLEMENTE CACHING
module.exports = async function () {
  let client = new mongo.MongoClient(config.mongoUrl);
  await client.connect();
  return client;
};
