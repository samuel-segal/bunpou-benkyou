const fs = require("fs");
const express = require("express");

const errorHandler = require("./middleware/errorHandler");
const cacheRes = require("./middleware/cacheRes");
const logger = require("./middleware/logger");

const router = express.Router();

router.use(logger.router);
router.use(cacheRes.router);

//Dynamically loads routers from api
const routerFiles = fs
  .readdirSync("./server/api")
  .filter((file) => file.endsWith(".js"));
for (const file of routerFiles) {
  const apiEndPoint = require(`./api/${file}`);
  console.log(`Loaded api endpoint: ${file}`);
  router.use(apiEndPoint.path, apiEndPoint.router);
}
router.use(errorHandler.router);

module.exports = router;
