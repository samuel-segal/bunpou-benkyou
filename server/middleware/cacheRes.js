const memCache = require("memory-cache");
const apiCache = new memCache.Cache();

const router = async function (req, res, next) {
  const url = req.originalUrl;

  const cachedJson = apiCache.get(url);

  if (cachedJson) {
    console.log("  Result returned from cache middleware");
    res.json(cachedJson);
  } else {
    console.log("  Result returned from api routers");
    next();
  }
};

const cacheRes = function (url, res) {
  apiCache.put(url, res);
};

module.exports = {
  router,
  cacheRes,
};
