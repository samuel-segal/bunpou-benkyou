module.exports.router = function (req, res, next) {
  const date = new Date();
  console.log(
    `[${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}] Data requested from [${
      req.connection.remoteAddress
    }] at ${req.originalUrl}`
  );
  next();
};
