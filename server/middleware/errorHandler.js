const router = function (err, req, res, next) {
  console.log(err);
};

const errorAsyncWrapper = function (asyncCallback) {
  return async function (req, res, next) {
    try {
      await asyncCallback(req, res, next);
    } catch (err) {
      next(err);
    }
  };
};

module.exports = {
  router,
  errorAsyncWrapper,
};
