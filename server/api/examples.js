const { ObjectId } = require("bson");
const express = require("express");
const db = require("../db/db.js");
const { cacheRes } = require("../middleware/cacheRes.js");
const { errorAsyncWrapper } = require("../middleware/errorHandler.js");
const router = express.Router();

router.get(
  "/:id/:numCall",
  errorAsyncWrapper(async function (req, res) {
    const id = req.params.id;
    const numCall = +req.params.numCall;

    //Utilizes dynamic collections
    const client = await db();
    const exampleDb = client.db("examples");
    const collection = exampleDb.collection(id);
    const exampleCursor = collection.find({});

    //Ensures that the returned objs are not more than there exists in the doc
    const count = await collection.countDocuments();
    const numQuery = Math.min(numCall, count);

    const exampleArr = [];
    //TODO Async this for speed
    for (let i = 0; i < numQuery; i++) {
      const example = await exampleCursor.next();
      exampleArr.push(example);
    }

    res.json(exampleArr);
    cacheRes(req.originalUrl, exampleArr);

    client.close();
  })
);

module.exports = {
  path: "/examples",
  router,
};
