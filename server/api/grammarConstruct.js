const { ObjectId } = require('bson');
const express = require('express');
const { LEGAL_TLS_SOCKET_OPTIONS } = require('mongodb');
const db = require('../db/db');
const { cacheRes } = require('../middleware/cacheRes');
const { errorAsyncWrapper } = require('../middleware/errorHandler');
const router = express.Router();

router.use('/:type/:id',errorAsyncWrapper(async function (req,res,next){
  const type = req.params.type;
  const id = req.params.id;
  if(!ObjectId.isValid(id)) return;
  const objectId = new ObjectId(id);

  const client = await db();
  const constructDb = client.db('grammar-rules');
  const collection = constructDb.collection(type);

  const constructObj = await collection.findOne({_id: objectId});
  res.json(constructObj);
  cacheRes(req.originalUrl,lessonObj);

}));

module.exports = {
  path: '/construct',
  router
}
