const { ObjectId } = require("bson");
const express = require("express");
const { nextTick } = require("process");

const db = require("../db/db.js");
const { cacheRes } = require("../middleware/cacheRes.js");
const { errorAsyncWrapper } = require("../middleware/errorHandler.js");
const router = express.Router();

router.get(
  "/:id",
  errorAsyncWrapper(async function (req, res) {
    const id = req.params.id;
    if (!ObjectId.isValid(id)) return;
    const objectId = new ObjectId(id);

    const client = await db();
    const articleDb = client.db("articles");
    const lessons = articleDb.collection("lessons");

    const lessonObj = await lessons.findOne({ _id: objectId });

    res.json(lessonObj);
    cacheRes(req.originalUrl, lessonObj);

    client.close();
  })
);

module.exports = {
  path: "/lessons",
  router,
};
